package io.github.escolarprogramming.Pr2_Bl3;

import java.util.Objects;

/**
 * Class for a single character
 *
 * @author Malte Schöppe
 * @version 2019-04-28
 */
public class Character extends AllCharacters.BaseCharacter {

    /**
     * Name of character
     */
    private final String name;

    /**
     * Wickedness of character
     */
    private double wickedness;

    /**
     * If character is dead
     */
    private boolean dead;

    /**
     * The love of the character
     */
    private Character loves;

    /**
     * Default constructor of the class
     *
     * @param name       The name of the character
     * @param wickedness The wickedness of the character
     */
    public Character(String name, double wickedness) {
        if (name == null) {
            throw new IllegalArgumentException("Name darf nicht leer sein");
        } else {
            this.name = name;
        }

        if (wickedness < 0 || wickedness > 1) {
            throw new IllegalArgumentException("Wert für Bösartigkeit muss zwischen 0 und 1 liegen");
        } else {
            this.wickedness = wickedness;
        }
    }

    /**
     * Get the name of the character
     *
     * @return Name of character
     */
    public String getName() {
        return name;
    }

    /**
     * Get the wickedness of the character
     *
     * @return Wickedness of character
     */
    public double getWickedness() {
        return wickedness;
    }

    /**
     * Set the wickedness of the character
     *
     * @param wickedness Wickedness of character
     */
    public void setWickedness(double wickedness) {
        if (wickedness < 0 || wickedness > 1) {
            throw new IllegalArgumentException("Wert für Bösartigkeit muss zwischen 0 und 1 liegen");
        }
        this.wickedness = wickedness;
    }

    /**
     * Check if the caracter if dead
     *
     * @return True if dead
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * Set the dead of the character
     *
     * @param dead Is dead or not
     */
    public void setDead(boolean dead) {
        this.dead = dead;
    }

    /**
     * Get the character which this character loves
     *
     * @return The love of this character
     */
    public Character getLoves() {
        return loves;
    }

    /**
     * Set the love of this character
     *
     * @param loves The new love
     */
    public void setLoves(Character loves) {
        this.loves = loves;
    }

    /**
     * Get the properties of the character in a string
     *
     * @return Properties string
     */
    @Override
    public String toString() {
        return "Character{name=" + name + ", wickedness=" + wickedness + ", dead=" + dead + ", loves=" + (loves == null ? "not in love" : loves.getName()) + "}";
    }

    /**
     * Check if this character has equal properties to given character or is the same
     *
     * @param object The other character to check
     * @return True if equals
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Character character = (Character) object;
        return (wickedness == character.wickedness) &&
                (dead == character.dead) &&
                name.equals(character.name) &&
                Objects.equals(loves, character.loves);
    }

    /**
     * Get the hash code of this character
     *
     * @return Hash code of this character
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, wickedness, dead, loves);
    }

    /**
     * Kills the given character
     *
     * @param victim The character to kill
     */
    private void kill(Character victim) {
        if (victim.isDead()) {
            throw new IllegalArgumentException("Einen toten Character kann man nicht noch einmal toeten");
        } else {
            victim.setDead(true);
        }
    }

    /**
     * Let this character fall in love with the given
     *
     * @param other The character to fall in love
     */
    public void fallInLove(Character other) {
        if (other == null) return;

        Character otherLove = other.getLoves();

        if (otherLove != null && otherLove != this) {
            if (otherLove.equals(this) || this.wickedness > 0.6) {
                this.kill(otherLove);
            }
        }

        this.setLoves(other);
    }
}
